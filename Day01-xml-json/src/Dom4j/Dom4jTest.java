package Dom4j;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.util.List;

public class Dom4jTest {
    public static void main(String[] args)throws Exception{
        //通过dom里的SAXReader读取xml文件
        Document doc = new SAXReader().read("Day01-xml-json/Demo03.xml");
        System.out.println(doc);

        //利用document获取xml文件的根节点
        Element rootElement = doc.getRootElement();
        System.out.println(rootElement.getName());

        System.out.println("==============");
        //获取根节点下的所有子节点
        List<Element> elements = rootElement.elements();
        for (Element productElemnet : elements) {
            System.out.println(productElemnet.getName() + " " + productElemnet.attributeValue("pid") + " " + productElemnet.attributeValue("color"));

            for (Element childElement : productElemnet.elements()) {
                System.out.println(childElement.getName() + " " + childElement.getText() + " " + childElement.attributeValue("name"));
            }
            System.out.println("=======");
        }

    }
}
