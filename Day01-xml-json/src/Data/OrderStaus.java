package Data;

public enum OrderStaus {
    CREATE_NEW(1000,"待付款"),
    PAYED(1001,"已付款"),
    SENDED(1002,"已发货");

    private Integer code;
    private String msg;

    OrderStaus(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
