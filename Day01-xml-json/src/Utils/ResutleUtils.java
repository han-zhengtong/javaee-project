package Utils;

import Data.ResponseData;
import cn.hutool.json.JSONUtil;

import java.lang.reflect.Method;




public class ResutleUtils {
    /**
     *
     * @param anEnum
     * @param data
     * @return
     * @param <T>
     */
    public static <T> String result(Enum anEnum,T data){
        try {
            ResponseData<Object> responseData = new ResponseData<>();
            responseData.setData(data);
            if (anEnum!=null) {
                Method getCode = anEnum.getClass().getMethod("getCode");
                Method getMsg = anEnum.getClass().getMethod("getMsg");
                if (getCode!=null){
                    responseData.setCode((Integer) getCode.invoke(anEnum));
                }
                if (getMsg!=null) {
                    responseData.setMsg((String) getMsg.invoke(anEnum));
                }
            }
            return JSONUtil.toJsonStr(responseData);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}
