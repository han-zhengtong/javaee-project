package org.example.test;

import org.example.constant.EnrollmentStatus;
import org.example.sms.MiddleStudent;
import  org.example.sms.PrimaryStudent;
import org.junit.Test;
import  org.example.sms02.StudentManagementSystem02;

import java.time.LocalDate;

import static org.example.constant.EnrollmentStatus.ENROLLED;

public class SMSTest03 {
    private StudentManagementSystem02 sms = new StudentManagementSystem02();

    @Test
    public void getPropertyValue() throws NoSuchFieldException, IllegalAccessException {
        PrimaryStudent ps = new PrimaryStudent(20241316, "小新", 77, 81
                , LocalDate.parse("2013-10-12"), "233434@qq.com");

        System.out.println(sms.getPropertyValue(ps,"name"));
    }

    @Test
    public void setPropertyValue() throws NoSuchFieldException, IllegalAccessException {
        PrimaryStudent ps = new PrimaryStudent(20241316, "小新", 77, 81
                , LocalDate.parse("2013-10-12"), "233434@qq.com");
        sms.setPropertyValue(ps,"name","小小新");

       System.out.println(sms.getPropertyValue(ps, "name"));
    }

    @Test
    public void getPropertyValue02() throws NoSuchFieldException, IllegalAccessException {
        MiddleStudent ms = new MiddleStudent(20241333, "大花", 60, 64
                , LocalDate.parse("2023-10-15"), "23343423@qq.com",ENROLLED, 78,91);

        System.out.println(sms.getPropertyValue(ms,"studentId"));
    }

}