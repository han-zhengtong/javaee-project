package org.example.test;

import org.junit.BeforeClass;
import org.junit.Test;
import org.example.sms02.StudentManagementSystem02;

import java.io.IOException;

public class SMSTest02 {
    private static StudentManagementSystem02 sms;

    @BeforeClass
    public static void initData() throws IOException {
        sms = new StudentManagementSystem02();
        sms.loadStudentsFromFile("G:\\OneDrive - JianXin\\课程研发\\Java\\JavaSE\\code" +
                "\\biwujiaoyu-javase-porject\\day25-junit-reflect-enum\\小学生信息.txt");
    }

    @Test
    public void showAllStudents() {
        sms.showAllStudents();
    }


    @Test
    public void writeRankedStudentsToFile() throws IOException {
        sms.writeRankedStudentsToFile("G:\\OneDrive - JianXin\\课程研发\\Java\\JavaSE\\code" +
                "\\biwujiaoyu-javase-porject\\day25-junit-reflect-enum\\总分排名.txt");
    }

    @Test
    public void getScoreStatisticsBySubject() {
        System.out.println(sms.getScoreStatisticsBySubject("语文"));
    }

    @Test
    public void findStudentWithHighestScore() {
        System.out.println(sms.findStudentWithHighestScore("数学"));
    }

    @Test
    public void sortByTotalScore() {
        sms.sortByTotalScore();
        sms.showAllStudents();
    }

    @Test
    public void randomRollCall() {
        System.out.println(sms.randomRollCall());
    }

}