package org.example.test;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class LoadTest {
    @Test
    public void test() throws IOException {
        //想实现让资源文件随着类的加载而加载
        //会从测试类的根路径加载: target/test-classes作为根目录,搜索指定的资源文件
        InputStream is = LoadTest.class.getClassLoader().getResourceAsStream("中学生信息02.txt");

        //利用转换流将字节流转换成字符流,在包装成缓冲流
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        System.out.println(br.readLine());
    }
}
