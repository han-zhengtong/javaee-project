package org.example.test;

import org.junit.Test;
import org.example.sms02.StudentManagementSystem02;

import java.io.IOException;

import static org.junit.Assert.*;

public class SMSTest04 {
    private StudentManagementSystem02 sms = new StudentManagementSystem02();

    @Test
    public void loadStudentsFromFile() throws IOException {
        sms.loadStudentsFromFile("小学生信息02.txt");
        sms.showAllStudents();
    }

    @Test
    public void loadStudentsFromFile02() throws IOException {
        sms.loadStudentsFromFile("中学生信息02.txt");
        sms.showAllStudents();
    }
}