package org.example.test;

import org.example.sms.PrimaryStudent;
import org.junit.Before;
import org.junit.Test;
import org.example.sms.Student;
import org.example.sms02.StudentManagementSystem02;

import java.time.LocalDate;
import java.util.Collection;

/**
 * 测试学生信息管理系统
 */
public class SMSTest01 {

    //将学生信息管理系统对象放在成员位置,目的是所有成员方法都可以使用
    StudentManagementSystem02 sms = new StudentManagementSystem02();

    /**
     * 初始化学生信息管理系统的数据,为将来测试方法做准备
     */
    @Before
    public void initData() {
        //1.添加学生
        sms.addStudent(new PrimaryStudent(20241316,"小新",77,81
                , LocalDate.parse("2013-10-12"),"233434@qq.com"));
        sms.addStudent(new PrimaryStudent(20241327,"小明",90,88
                , LocalDate.parse("2014-11-12"),"233534@qq.com"));
        sms.addStudent(new PrimaryStudent(20241333,"花花",90,64
                , LocalDate.parse("2023-10-15"),"233434@qq.com"));
        sms.addStudent(new PrimaryStudent(20241335,"花花",71,83
                , LocalDate.parse("2024-12-12"),"123123323@qq.com"));
    }

    @Test
    public void showAllStudents() {
        sms.showAllStudents();
    }

    @Test
    public void findStudentById() {
        System.out.println(sms.findStudentById(20241333));
    }

    @Test
    public void getAverageScoreBySubject() {
        System.out.println(sms.getAverageScoreBySubject("语文"));
    }

    @Test
    public void getStudentsEnrolledBefore() {
        Collection<Student> smsStudentsEnrolledBefore = sms.getStudentsEnrolledBefore("2023-11-01");
        sms.showAllStudents(smsStudentsEnrolledBefore);
    }


}
