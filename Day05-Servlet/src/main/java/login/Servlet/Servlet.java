package login.Servlet;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import login.Utils.JDBCUtils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                //1.获取浏览器发送的参数通过request
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        //利用用户名密码在数据库中查询
        Connection conn = JDBCUtils.getConnection();
        ResultSet resultSet;
        try {
            PreparedStatement statement = conn.prepareStatement("SELECT * FROM `user` WHERE username = ? AND password = ?");
            statement.setString(1,username);
            statement.setString(2,password);
            resultSet = statement.executeQuery();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        try {
            if ( resultSet.next() !=false){
                response.getWriter().write("<font color='red'>True</font>");
            }else {
                response.getWriter().write("<font color='red'>false</font>");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }
}
