package Servlet_Demo;

import jakarta.servlet.*;

import java.io.IOException;

public class Demo01 implements Servlet {
    public void init(ServletConfig servletConfig) throws ServletException {

    }

    public ServletConfig getServletConfig() {
        return null;
    }

    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
              System.out.println("Hello Servlet");
              servletResponse.getWriter().write("<font color='red'>Hello servlet</font>");
    }

    public String getServletInfo() {
        return null;
    }

    public void destroy() {

    }
}
